<?php

/**
 * @file
 * Page related functions for the hosting_task_graphs module.
 */

/**
 * Callback for the generic task graph page.
 *
 * @param $task_runs
 *   An array of task runs which contains the following keys for each run:
 *   executed (start time), delta,

 */
function hosting_task_graphs_page($task_id, $task_runs = array()) {
  if (empty($task_runs)) {
     return t('No previous task runs found.');
  }
  $points = array();
  foreach ($task_runs as $item) {
    $points[] = array(
      // Multiply the start to put timestamp into ms instead of s
      $item['executed'] * 1000, $item['delta']
    );
  }

  $element = array(
     'id' => 'float-task-graph-' . drupal_html_class($task_id),
  );
  $data = array(
     array(
        'label' => t('Task execution times'),
        'data' => $points,
     ),
  );
  $options = array(
    'series' => array(
      'lines' => array(
        'show' => TRUE,
      ),
      'points' => array(
        'show' => TRUE,
      ),
    ),
    'xaxis' => array(
      'mode' => 'time', // Need to make sure float.time.js gets included
    ),
  );
  $variables = array(
     'element' => $element,
     'data' => $data,
     'options' => $options,
  );
  $graph = theme('flot_graph', $variables);

  // Call this after theme, so we can ensure that jquery.flot.js is included first.
  drupal_add_js(_flot_get_library_path() . '/jquery.flot.time.js');

  return $graph;
}

/**
 * Page callback for the site's tasks.
 *
 * @param $node
 *   Loaded node object for the site.
 *
 * @returns string
 *   Renderered content.
 */
function hosting_task_graphs_site_task_graphs_page($node) {
   $query = db_select('node', 'n');
   $query->join('hosting_task', 't', 'n.nid = t.rid');
   $query->fields('n', array('nid', 'vid', 'title'))
      ->fields('t', array('task_type', 'executed', 'delta', 'task_status'))
      ->condition('n.nid', $node->nid)
      ->isNotNull('t.executed')
      ->orderBy('t.executed');
   $results = $query->execute();
   $tasks = array();
   foreach ($results as $row) {
      $tasks[$row->task_type][] = (array)$row;
   }
   foreach ($tasks as $task_type => $task_runs) {
      $content["task_type_{$task_type}"] = array(
         "task_type_{$task_type}_title" => array(
            '#markup' => t('Task history for !title tasks', array('!title' => $task_type)),
            '#prefix' => '<h3>',
            '#suffix' => '</h3>',
          ),
         "task_type_{$task_type}_graph" => array(
            '#markup' => hosting_task_graphs_page("{$node->nid}_{$task_type}",$task_runs),
         ),
      );
   }
   if (empty($content)) {
      return t('No tasks have been run for this site yet.');
   }
   return drupal_render($content);
}
