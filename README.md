Adds graphs to visualize the time take over runs of tasks.

Once installed, go to hosting/task/TASK_ID/graph to see the graph.

# Requirements

  * hosting_task (aegirproject.org)
  * flot (drupal.org/project/flot)
    * this requires the flot library, available at http://www.flotcharts.org/
  * (Recommended, but not required) libraries

# Installation

  * Download and install flot in the module search path of your aegir front-end site
  * Download and unpack the flot library in site/SITENAME/libraries (if using libraries), or sites/SITENAME/modules/flot/ otherwise
  * Place this module in the module search path of your aegir font-end site
  * Enable hosting_task_graphs

# Configuration

None


# Usage

Go to hosting/task/TASK_ID/graph

